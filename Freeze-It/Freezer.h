//
//  Freezer.h
//  Freeze-It
//
//  Created by Kynaston Pomlett on 03/03/2014.
//  Copyright (c) 2014 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Freezer : NSManagedObject

@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSOrderedSet *location;
@property (nonatomic, retain) NSSet *item;
@end

@interface Freezer (CoreDataGeneratedAccessors)

- (void)insertObject:(NSManagedObject *)value inLocationAtIndex:(NSUInteger)idx;
- (void)removeObjectFromLocationAtIndex:(NSUInteger)idx;
- (void)insertLocation:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeLocationAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInLocationAtIndex:(NSUInteger)idx withObject:(NSManagedObject *)value;
- (void)replaceLocationAtIndexes:(NSIndexSet *)indexes withLocation:(NSArray *)values;
- (void)addLocationObject:(NSManagedObject *)value;
- (void)removeLocationObject:(NSManagedObject *)value;
- (void)addLocation:(NSOrderedSet *)values;
- (void)removeLocation:(NSOrderedSet *)values;
- (void)addItemObject:(NSManagedObject *)value;
- (void)removeItemObject:(NSManagedObject *)value;
- (void)addItem:(NSSet *)values;
- (void)removeItem:(NSSet *)values;

@end
