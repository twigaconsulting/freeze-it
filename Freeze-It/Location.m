//
//  Location.m
//  Freeze-It
//
//  Created by Kynaston Pomlett on 03/03/2014.
//  Copyright (c) 2014 Twiga Consulting Ltd. All rights reserved.
//

#import "Location.h"
#import "Freezer.h"
#import "Item.h"


@implementation Location

@dynamic id;
@dynamic name;
@dynamic freezer;
@dynamic item;

@end
