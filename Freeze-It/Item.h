//
//  Item.h
//  Freeze-It
//
//  Created by Kynaston Pomlett on 03/03/2014.
//  Copyright (c) 2014 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Freezer;

@interface Item : NSManagedObject

@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Freezer *freezer;
@property (nonatomic, retain) NSManagedObject *location;

@end
