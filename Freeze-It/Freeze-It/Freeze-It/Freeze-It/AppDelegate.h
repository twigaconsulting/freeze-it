//
//  AppDelegate.h
//  Freeze-It
//
//  Created by Kynaston Pomlett on 03/03/2014.
//  Copyright (c) 2014 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
