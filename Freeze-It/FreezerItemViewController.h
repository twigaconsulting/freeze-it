//
//  FreezerItemViewController.h
//  Freeze-It
//
//  Created by Peter Pomlett on 03/03/2014.
//  Copyright (c) 2014 Twiga Consulting Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FreezerItemViewController : UITableViewController

@end
