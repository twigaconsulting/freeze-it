//
//  Item.m
//  Freeze-It
//
//  Created by Kynaston Pomlett on 03/03/2014.
//  Copyright (c) 2014 Twiga Consulting Ltd. All rights reserved.
//

#import "Item.h"
#import "Freezer.h"


@implementation Item

@dynamic id;
@dynamic name;
@dynamic freezer;
@dynamic location;

@end
