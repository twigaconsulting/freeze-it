//
//  Freezer.m
//  Freeze-It
//
//  Created by Kynaston Pomlett on 03/03/2014.
//  Copyright (c) 2014 Twiga Consulting Ltd. All rights reserved.
//

#import "Freezer.h"


@implementation Freezer

@dynamic id;
@dynamic name;
@dynamic location;
@dynamic item;

@end
