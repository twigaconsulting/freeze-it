//
//  Location.h
//  Freeze-It
//
//  Created by Kynaston Pomlett on 03/03/2014.
//  Copyright (c) 2014 Twiga Consulting Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Freezer, Item;

@interface Location : NSManagedObject

@property (nonatomic, retain) NSString * id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Freezer *freezer;
@property (nonatomic, retain) NSSet *item;
@end

@interface Location (CoreDataGeneratedAccessors)

- (void)addItemObject:(Item *)value;
- (void)removeItemObject:(Item *)value;
- (void)addItem:(NSSet *)values;
- (void)removeItem:(NSSet *)values;

@end
